# Getting started

## Step-1: Start Keycloak server on 8180 port

```bash
# Start Keycloak server
cd /Users/chunywan/work/keycloak-apps/keycloak-4.0.0.Beta1/bin
./standalone.sh -Djboss.socket.binding.port-offset=100

# http://localhost:8180/auth/
# admin/password
```

## Step-2: Setup realm, client and user from http://localhost:8180/auth/

- realm: demo
- add clients: 'app-cc-console-demo' (http://localhost:4200)
- add user: user1/password
- add role: create role named "user" and assign to user1 above. 

## Step-3: Start  'app-cc-console-demo'
```bash
npm start
```

## Step-4: access app-cc-console-demo
http://localhost:4200
