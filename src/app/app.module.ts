import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {HttpModule} from "@angular/http";
import {KeycloakService} from "./keycloak-service/keycloak.service";
import {KEYCLOAK_HTTP_PROVIDER} from "./keycloak-service/keycloak.http";


@NgModule({
  declarations: [ AppComponent ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [
    KeycloakService,
    KEYCLOAK_HTTP_PROVIDER
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
